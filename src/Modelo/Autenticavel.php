<?php

namespace Alura\Banco\Modelo;
require_once 'autoload.php';

interface Autenticavel
{
    public function podeAutenticar(string $senha): bool;
}