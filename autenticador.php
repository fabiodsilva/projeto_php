<?php

use Alura\Banco\Modelo\Cpf;
use Alura\Banco\Modelo\Funcionario\Diretor;
use Alura\Banco\Service\Autenticador;

require_once 'autoload.php';

$autenticador = new Autenticador();
$umDiretor = new \Alura\Banco\Modelo\Funcionario\Gerente(
    'João da Silva',
    new Cpf('123.456.789-10'),
    'Desenvolvedor',
    10000
);

$autenticador->tentaLogin($umDiretor, '4321');