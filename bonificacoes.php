<?php

require_once 'autoload.php';
use Alura\Banco\Service\ControladorDeBonificacoes;
use Alura\Banco\Modelo\Cpf;
use Alura\Banco\Modelo\Funcionario\{Funcionario,Gerente};


$umFuncionario = new Funcionario(
    'Vinicius Dias',
    new Cpf('123.456.789-10'),
    'Desenvolvedor',
    1000
);

$umaFuncionaria = new Gerente(
    'Patricia',
    new Cpf('987.654.321-10'),
    'Gerente',
    3000
);

$controlador = new ControladorDeBonificacoes();
$controlador->adicionaBonificacaoDe($umFuncionario);
$controlador->adicionaBonificacaoDe($umaFuncionaria);

echo $controlador->recuperaTotal() . PHP_EOL;
if ($umaFuncionaria->podeAutenticar('4321'))
    {
        echo 'Autenticou';
    }
else
    {
        echo 'Não Autenticou';
    }
echo PHP_EOL;
